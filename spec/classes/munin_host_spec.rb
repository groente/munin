require 'spec_helper'

describe 'munin::host' do
  let :pre_condition do
    'include munin'
  end

  let :default_facts do
    {
      :interfaces => 'eth0',
      :kernel => 'Linux',
      :virtual => 'physical',
      :acpi_available => 'absent',
    }
  end

  shared_examples 'debian-host' do |os, majrelease|
    let :facts do
      {
        :operatingsystem => os,
        :operatingsystemmajrelease => majrelease,
        :osfamily => 'Debian',
        :selinux => 'false',
        :concat_basedir => '/var/lib/puppet/concat',
      }.merge(default_facts)
    end
    it { should contain_package('munin') }
    it { should contain_concat('/etc/munin/munin.conf') }
    it { should contain_class('munin::host') }
  end

  shared_examples 'redhat-host' do |os, majrelease|
    let :facts do
      {
        :operatingsystem => os,
        :operatingsystemmajrelease => majrelease,
        :osfamily => 'RedHat',
        :selinux => 'true',
        :concat_basedir => '/var/lib/puppet/concat',
      }.merge(default_facts)
    end
    it { should contain_package('munin') }
    it { should contain_concat('/etc/munin/munin.conf') }
    it { should contain_class('munin::host') }
  end

  context 'on debian-like system' do
    it_behaves_like 'debian-host', 'Debian', '9'
    it_behaves_like 'debian-host', 'Debian', '10'
    it_behaves_like 'debian-host', 'Ubuntu', '20'
  end

  context 'on redhat-like system' do
    it_behaves_like 'redhat-host', 'CentOS', '6'
  end
  
  context 'on Gentoo' do
    let :facts do
      {
        :operatingsystem => 'Gentoo',
        :operatingsystemmajrelease => '',
        :osfamily => 'Gentoo',
        :selinux => 'false',
        :concat_basedir => '/var/lib/puppet/concat',
      }.merge(default_facts)
    end
    it { should contain_package('munin') }
    it { should contain_concat('/etc/munin/munin.conf') }
    it { should contain_class('munin::host') }
  end
end
